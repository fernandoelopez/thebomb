#include <stdint.h>
#include "net.h"

error_t remote_send_message(
        RHReliableDatagram *rhd,
        remote_message_t *message,
        uint16_t timeout)
{
    bool sent;
    sent = rhd->sendtoWait((uint8_t *)message, sizeof(remote_message_t), STATION_ADDR);
    if (!sent) return RH_TRANSMISSION_ERROR;
    return _OK;
}

error_t remote_recv_response(
        RHReliableDatagram *rhd,
        station_response_t *response,
        uint16_t timeout)
{
    uint8_t len = sizeof(station_response_t);
    uint8_t from, to, id, flags;
    Serial.print(len); Serial.print("<- Expected len, ");
    bool success = rhd->recvfromAckTimeout(
        (uint8_t *)response,
        &len,
        timeout,
        &from, &to, &id, &flags
    );
    if (!success) {
        Serial.print(len); Serial.print(", ");
        Serial.print(from); Serial.print(", ");
        Serial.print(to); Serial.print(", ");
        Serial.print(id); Serial.print(", ");
        Serial.print(flags); Serial.print(", ");
        Serial.println();
        return RH_RECV_ERROR;
    }
    if (len != sizeof(station_response_t)){
        return MESSAGE_LEN_MISMATCH;
    }
    return _OK;
}
