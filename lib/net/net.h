#ifndef FORMAT_H
#define FORMAT_H
#include <RHGenericDriver.h>
#include <RHReliableDatagram.h>

/*
_________________________________________
Remote         |  Station
---------------+-------------------------
hi            -->
              <-- State & random seq
state & seq+1 -->
              <-- State & seq+1
*/

#define REMOTE_ADDR 2
#define STATION_ADDR 4

#include <Arduino.h>

typedef enum {
    HI,
    TURN_ON,
    TURN_OFF,
} command_t;

typedef enum {
    STATION_MOTOR_ON,
    STATION_MOTOR_OFF,
    STATION_TANK_FULL,
} state_t;

typedef enum {
    _OK = 0,
    UNKNOWN_ERROR = -128,
    RH_TRANSMISSION_ERROR,
    RH_RECV_ERROR,
    MESSAGE_LEN_MISMATCH,
} error_t;

typedef struct __attribute__((packed)){
    uint16_t seq;
    uint8_t command;
    uint8_t salt;
    uint8_t crc;
} remote_message_t;

typedef struct __attribute__((packed)){
    uint16_t seq;
    state_t state;
} station_response_t;

error_t remote_send_message(
    RHReliableDatagram *rhd,
    remote_message_t *message,
    uint16_t timeout
);

error_t remote_recv_response(
    RHReliableDatagram *rhd,
    station_response_t *response,
    uint16_t timeout
);

error_t station_send_response(
    RHReliableDatagram *rhd,
    station_response_t *response,
    uint16_t timeout
);

error_t station_recv_message(
    RHReliableDatagram *rhd,
    remote_message_t *message,
    uint16_t timeout
);

#endif
