#include <net.h>
#include <RH_NRF24.h>
#include <RHReliableDatagram.h>
#include <Arduino.h>

#ifdef REMOTE
//#define GREEN_LED 4
#define RED_LED 3
#define GREEN_LED 2
#define BUZZER 5
#define CHIP_ENABLE 7
#define SLAVE_SELECT 8
#define SELF_ADDR REMOTE_ADDR
#else
#define GREEN_LED 3 // NC
// IRQ 2
#define CHIP_ENABLE 7
#define SLAVE_SELECT 8
#define SELF_ADDR STATION_ADDR
#endif

remote_message_t rmessage;
station_response_t sresponse;
RH_NRF24 radio(CHIP_ENABLE, SLAVE_SELECT);
RHReliableDatagram rh_conn(radio, SELF_ADDR);

#define show_error(error) show_error_fn(__LINE__, (error))
void show_error_fn(unsigned line, error_t error){
    Serial.print("Error: ");
    Serial.print(error);
    Serial.print(", " __FILE__);
    Serial.print(":");
    Serial.println(line);
}

#ifdef DEBUG
#define debug(msg) debug_fn(__LINE__, (msg))
void debug_fn(unsigned line, const char *msg){
    Serial.print("DEBUG: ");
    Serial.print(msg);
    Serial.print(", " __FILE__);
    Serial.print(":");
    Serial.println(line);
}
#else
#define debug(msg)
#endif


void setup(){
    Serial.begin(115200);
    pinMode(GREEN_LED, OUTPUT);
    rh_conn.init();
}

#ifdef REMOTE
void loop(){
    error_t error;
    state_t intended_state;
    state_t reported_state;
    static uint16_t seq;
    rmessage.seq = 0;
    rmessage.command = HI;
    rmessage.salt = analogRead(A0);
    rmessage.crc = 0;
remote_send:
    rmessage.seq++;
    digitalWrite(GREEN_LED, HIGH);
    debug("Remote sends message");
    error = remote_send_message(&rh_conn, &rmessage, 500);
    if (error != _OK){
        show_error(error);
        delay(500);
        goto remote_send;
    }
    digitalWrite(GREEN_LED, LOW);
    debug("Remote prepares to receive response");
    error = remote_recv_response(
        &rh_conn,
        &sresponse,
        500
    );
    if (error != _OK){
        show_error(error);
        return;
    }
    debug("Remote received message successfully");
    seq = sresponse.seq;
    reported_state = sresponse.state;

    intended_state = (reported_state == STATION_MOTOR_ON)?STATION_MOTOR_OFF:STATION_MOTOR_ON;
    rmessage.seq = ++seq;
    rmessage.command = (intended_state == STATION_MOTOR_ON)?TURN_ON:TURN_OFF;
    rmessage.salt = analogRead(A0);
    rmessage.crc = 0;
    error = remote_send_message(&rh_conn, &rmessage, 500);
    if (error != _OK){
        show_error(error);
        return;
    }

    digitalWrite(RED_LED, HIGH);
    error = remote_recv_response(&rh_conn, &sresponse, 500);
    if (error != _OK){
        show_error(error);
        return;
    }
    if (sresponse.state == STATION_MOTOR_ON) {
        //tone(BUZZER, 500, 200);
        //delay(250);
    }
    delay(500);
    digitalWrite(RED_LED, LOW);
}
#endif

#ifdef STATION
void loop(){
    remote_message_t msg;
    station_response_t resp;
    error_t error;

    debug("Station waits for message");
    digitalWrite(GREEN_LED, HIGH);
    error = station_recv_message(&rh_conn, &msg, 500);
    digitalWrite(GREEN_LED, LOW);
    if (error != _OK){
        show_error(error);
        return;
    }
    debug("Station received message successfully");
    Serial.println(msg.seq);
    Serial.println(msg.command);
    Serial.println("--");

    resp.seq = 42;
    resp.state = (state_t) analogRead(A0) % 2;
    debug("Station sends response");
    station_send_response(&rh_conn, &resp, 200);
    debug("Station sent the response but didn't check if arrived");
}
#endif
